import os
import stat


class File(object):
    def __init__(self, directory):
        st_result = os.lstat(directory)
        self.mode = st_result.st_mode
        self.filename = os.path.basename(directory)
        self.size = st_result.st_size
        self.mtime = 5

    def __getitem__(self, name):
        if name == 'filename':
            return self.get_filename()
        elif name == 'filesize':
            return self.get_size()
        elif name == 'mode':
            return self.get_mode()
        else:
            raise KeyError

    def get_filename(self):
        return self.filename

    def get_mode(self):
        return stat.filemode(self.mode)
    
    def get_size(self):
        prefixes = iter('BkMGT')
        prefix = next(prefixes)
        size = self.size
        while size >= 800:
            size /= 1024
            prefix = next(prefixes)
        return '{:4.3g}{}'.format(size, prefix)

    def get_size3(self, limit):
        base = 1000
        prefixes = iter('bkmgtp')
        prefix = next(prefixes)
        div, mod, mantissa = self.size, 0, 0
        while div >= 100:
            div, mod = divmod(div, base)
            prefix = next(prefixes)
        if div == 0:
            mantissa = mod * 100 // base
            return '{}{:02d}'.format(prefix, mantissa)
        elif div < 10:
            mantissa = mod * 10 // base
            return '{}{}{}'.format(div, prefix, mantissa)
        else:
            return '{}{}'.format(div, prefix)
