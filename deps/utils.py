from functools import partial


def inject(*args, **kwargs):
    return partial(partial, *args, **kwargs)


class Commands(object):
    commands = {}

    def __init__(self):
        self.bindings = {}
        self.load_config()
    
    @classmethod
    def _translate_name(cls, name):
        return name.replace('_', '-')
    
    @staticmethod
    @inject(commands=commands)
    def add(commands, name=None):
        def wrapper(method):
            commands[name] = method

            def caller(*args, **kwargs):
                return method(*args, **kwargs)
            return caller
        return wrapper

    def _execute_command(self, name):
        method = self.commands.get(name)
        return method(self)

    def load_config(self, filename='config'):
        with open(filename, 'r') as f:
            for line in f.readlines():
                key, command = line.split()
                self.bindings[key] = command


def get_valid_sized_string(string, limit, truncate=False):
    if truncate:
        if limit == 0:
            string = ''
        elif len(string) > limit:
            string = string[:limit-1] + '~'
    if len(string) > limit:
        raise Exception('Output string ({}) too long (limit={})'
                        .format(string, limit))
    return string


def space(config_dict, limit, spacing=0, offset=0, expand=False):
    """
    tries to fill space with elements
    config_dict:
    {key: (mode, value), ...}
    mode can be either:
    f -- means fixed size (value indicates the exact size)
    p -- means proportional (size is calculated depending on other elements, 
    where value indicates the proportional coefficient)
    return:
    {key: {'pos': pos, 'size', size}, ...}
    """
    f_sum = 0
    p_sum = 0
    out_dict = {}
    for name, (mode, value) in config_dict.items():
        if value <= 0:
            raise ValueError('Value has to be positive')
        if mode == 'p':
            p_sum += value
        elif mode == 'f':
            f_sum += value
        else:
            raise KeyError('Wrong mode ({})'.format(mode))

    remaining_e = limit - (f_sum + (len(config_dict)-1) * spacing)
    remaining_p = p_sum
    c_pos = offset

    if remaining_e < 0:
        if expand:
            limit -= remaining_e
            remaining_e = 0
        else:
            raise Exception('Limit exceeded ({}>{})'
                            .format(limit-remaining_e, limit))

    for name, (mode, value) in config_dict.items():
        if mode == 'f':
            out_dict[name] = {
                'size': value,
                'pos': c_pos
            }
            c_pos += value + spacing
        else:
            div, mod = divmod(remaining_e * value, remaining_p)
            size = div + 1 if mod * 2 >= remaining_p else div
            if size > remaining_e:
                raise Exception('Error spacing elements')
            out_dict[name] = {
                'size': size,
                'pos': c_pos
            }
            remaining_e -= size
            remaining_p -= value
            c_pos += size + spacing
    if remaining_p or remaining_e:
        raise Exception('Error spacing elements')
    return out_dict
    

class ItemsManager(object):
    def __init__(self, iterable=None, active_index=0):
        self._active_index = None
        self._active_item = None
        self._list = []
        if iterable:
            self._list = list(iterable)
            self._active_index = 0
            self.select_relative(active_index)
            self._recalc_active_item()    

    def __iter__(self):
        return iter(self._list)

    def __getitem__(self, sliced):
        return self._list[sliced]

    def __len__(self):
        return len(self._list)
    
    def get_active_index(self):
        return self._active_index

    def get_active_item(self):
        if self._active_index is not None:
            return self._active_item
        return None
        
    def _recalc_active_index(self):
        if self._list:
            gen = (ind for ind, it in enumerate(self._list)
                   if it == self._active_item)
            self._active_index = next(gen, 0)
        else:
            self._active_index = None
            
    def _recalc_active_item(self):
        if self._list:
            self._active_item = self._list[self._active_index]
            
    def select_relative(self, step, wrap=False):
        if self._active_index is not None:
            sel = self._active_index + step
            if wrap:
                sel %= len(self._list)
            else:
                sel = max(0, min(sel, len(self._list) - 1))  # clamp
            self._active_index = sel
            self._recalc_active_item()

    def select_first(self):
        if self._active_index:
            self._active_index = 0
            self._recalc_active_item()

    def select_last(self):
        if self._active_index:
            self._active_index = len(self._list) - 1
            self._recalc_active_item()
            
    def append(self, *args, **kwargs):
        self._list.append(*args, **kwargs)
