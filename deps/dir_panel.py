import os
from functools import partial

from .file import File
from .panel import GenericPanel
from .utils import ItemsManager

CONFIG = {
    'mode': ('f', 10),
    'filename': ('p', 2),
    'filesize': ('f', 6),
}

HEADERS_NAMES = {
    'mode': 'MODE',
    'filename': 'NAME',
    'filesize': 'SIZE',
}

DISPLAYED = [
    'mode',
    'filename',
    'filesize',
]

class DirPanel(GenericPanel):
    def __init__(self, cwd):
        super().__init__()
        self.title = cwd
        self.headers_config = CONFIG
        self.headers_names = HEADERS_NAMES
        self.displayed_headers = DISPLAYED

        self.cwd = cwd
        self.files = []
        self.probe()

    def set_cwd(self, cwd):
        self.cwd = cwd

    def visit_active_item(self):
        pass
        
    def probe(self):
        filenames = os.listdir(self.cwd)
        abs_filenames = map(partial(os.path.join, self.cwd), filenames)
        self.items = ItemsManager(map(File, abs_filenames))
