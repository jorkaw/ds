import curses
import curses.textpad
import os
import sys
from collections import OrderedDict

from .dir_panel import DirPanel
from .utils import Commands, space, ItemsManager


# todo: CommandBar class, with custom handling of user input

# redraw -- draw from scratch (clear the actual terminal, noticeable flickering) -- calling 'clear'
# update -- curses optimized draw (change only what's needed, no flickering) -- calling 'erase'


class Program(Commands):
    def __init__(self):
        super(Program, self).__init__()
        self.screen = None
        self.lwin = DirPanel(os.getcwd())
        self.rwin = DirPanel(os.path.expanduser('~'))
        self.lwin.active = True  # fixme: use ItemsManager
        self.panels = ItemsManager([self.lwin, self.rwin])
        self.command_bar = None

    def _init_curses(self):
        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.screen.keypad(1)
        curses.curs_set(0)
        
        for panel in self.panels:
            panel.init_window()

        self.command_bar = curses.newwin(0, 0)
        self.resize_fit()

    def _deinit_curses(self):
        self.screen.keypad(0)
        curses.curs_set(1)
        curses.echo()
        curses.nocbreak()
        curses.endwin()

        for panel in self.panels:
            panel.deinit_window()
        self.command_bar = None
        self.screen = None

    def resize_fit(self):
        y, x = self.screen.getmaxyx()
        config = OrderedDict((i, ('p', 1)) for i in range(len(self.panels)))
        layout = space(config, x)
        for i, panel in enumerate(self.panels):
            size = layout[i]['size']
            pos = layout[i]['pos']
            panel.resize_window(y-1, size, 0, pos)
        # x = 80 if x < 80 else x
        self.command_bar.resize(1, x)
        self.command_bar.mvwin(y-1, 0)
        self._update_all(redraw=True)

    def _run(self):
        while True:
            ch = self._getch()
            key_name = curses.keyname(ch).decode('ascii')
            command = self.bindings.get(key_name)
            self.command_bar.erase()
            if command:
                self.command_bar.addstr(0, 0, 'Executing `{}\'...'
                                        .format(command))
                self.command_bar.refresh()
                self._execute_command(command)
                if not self.command_bar.is_wintouched():
                    self.command_bar.erase()
                    self.command_bar.addstr(0, 0, 'OK')
            else:
                self._handle_undefined_binding(ch)
            self.command_bar.refresh()
            
    def run(self):
        try:
            self._init_curses()
            return self._run()
        finally:
            if self.screen is not None:
                self._deinit_curses()
                
    def _getch(self):
        ch = self.screen.getch()
        while ch == curses.KEY_RESIZE:
            self.resize_fit()
            ch = self.screen.getch()
        return ch
                
    @Commands.add(name='execute-command')
    def execute_command(self):
        command = self._get_input_from_command_bar('Command?')
        if command in Commands.commands:
            self._execute_command(command)
        else:
            self.command_bar.addstr(0, 0, "No such command: `{}'"
                                    .format(command))
            
    @Commands.add(name='quit')
    def quit(self):
        sys.exit()

    def _update_all(self, redraw=False):
        if redraw:
            self.screen.clear()
            self.command_bar.clear()
        else:
            self.screen.erase()
            self.command_bar.erase()
        self.screen.noutrefresh()
        self.command_bar.noutrefresh()

        for panel in self.panels:
            panel.update_window(redraw=redraw)

        curses.doupdate()

    @Commands.add(name='redraw')
    def redraw_all(self):
        self._update_all(redraw=True)

    @Commands.add(name='test')
    def test(self):
        pass
        
    @Commands.add(name='test-clear')
    def test(self):
        self.panels.get_active_item().debug_clear()
    
    @Commands.add(name='debug-keystroke')
    def debug_keystroke(self):
        self.panels.get_active_item().debug_clear()
        self.panels.get_active_item().debug_write(2, 3, 'key?')
        ch = self._getch()
        keyname = curses.keyname(ch).decode('ascii')
        self.panels.get_active_item().debug_clear()
        self.panels.get_active_item().debug_write(3, 3, str(ch))
        self.panels.get_active_item().debug_write(4, 3, keyname)

    @Commands.add(name='help')
    def help(self):
        self.screen.erase()
        row = 2
        for key, command in self.bindings.items():
            broken = '' if Commands.commands.get(command) else ' (broken)'
            self.screen.addstr(row, 5, "Key {} is bound to `{}'{}"
                               .format(key, command, broken))
            row += 1
        self.screen.addstr(row + 1, 5, 'Press any key to continue...')
        self.screen.refresh()
        self._getch()
        self._update_all()

    @Commands.add(name='next-item')
    def next_item(self):
        self.panels.get_active_item().next_item()

    @Commands.add(name='prev-item')
    def prev_item(self):
        self.panels.get_active_item().prev_item()

    @Commands.add(name='next-screen')
    def next_item(self):
        self.panels.get_active_item().next_screen()

    @Commands.add(name='prev-screen')
    def prev_item(self):
        self.panels.get_active_item().prev_screen()
        
    @Commands.add(name='next-panel')
    def next_panel(self):
        self.panels.get_active_item().active = False
        self.panels.select_relative(1, wrap=True)
        self.panels.get_active_item().active = True
        self._update_all()

    @Commands.add(name='visit-active-item')
    def visit_active_item(self):
        self.panels.get_active_item().visit_active_item()
    
    def _handle_undefined_binding(self, ch):
        keyname = curses.keyname(ch).decode('ascii')
        self.command_bar.addstr(0, 0, '`{}\' is undefined'.format(keyname))
        
    def _get_input_from_command_bar(self, prompt):
        def validator(x):
            if x == 127:
                return curses.KEY_BACKSPACE
            if x == 4:
                return curses.KEY_DC
            if x == ord(' '):
                return ord('-')
            return x
        self.command_bar.erase()
        self.command_bar.addstr(0, 0, prompt)
        self.command_bar.refresh()
        input_win = self.command_bar.derwin(0, len(prompt) + 1)
        textpad = curses.textpad.Textbox(input_win, insert_mode=True)
        curses.curs_set(1)
        ret = textpad.edit(validator)
        curses.curs_set(0)
        self.command_bar.erase()
        self.command_bar.refresh()
        return ret.strip()
