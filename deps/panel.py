import curses
from collections import OrderedDict

from .utils import space, get_valid_sized_string, ItemsManager


# todo: scrolling capabilities


class GenericPanel(object):
    def __init__(self):
        self.out_win = None  # outside, entire window
        self.cont_win = None  # contents' window

        self._attrib = curses.A_DIM
        self.headers_config = {}  # predefined config
        self.headers_layout = {}  # current field positions and sizes
        self.headers_names = {}  # displayed names
        self.displayed_headers = []
        self.items = ItemsManager()        
        self.first_displayed = 0  # for scrolling purposes
        
        self.title = ''
        self._active = False

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, value):
        self._active = value
        self._attrib = curses.A_NORMAL if value else curses.A_DIM
        if self.out_win:
            self.out_win.bkgdset(' ', self._attrib)
            self.cont_win.bkgdset(' ', self._attrib)
        
    @active.getter
    def active(self):
        return self._active    

    def get_cont_sz(self):
        y, x = self.cont_win.getmaxyx()
        return y, x
    
    def init_window(self):
        self.out_win = curses.newwin(0, 0)
        self.cont_win = self.out_win.derwin(0, 0, 2, 1)
        self.out_win.bkgdset(' ', self._attrib)  # fixme: attrset?
        self.cont_win.bkgdset(' ', self._attrib)

    def deinit_window(self):
        self.cont_win = None

    def get_current_config(self):
        return OrderedDict((field, self.headers_config[field])
                           for field in self.displayed_headers)

    def resize_window(self, sz_y, sz_x, pos_y, pos_x):
        config = self.get_current_config()
        self.headers_layout = space(config, sz_x - 2, 1, 0)
        self.out_win.resize(sz_y, sz_x)
        self.out_win.mvwin(pos_y, pos_x)
        self.cont_win.resize(sz_y - 3, sz_x - 2)
        self.cont_win.mvwin(pos_y + 2, pos_x + 1)
        
    def _set_title(self, title):
        title = title or '(untitled)'
        self.out_win.addstr(0, 2, ' {} '.format(title))

    def _clear_window(self, title=None, redraw=False):
        if redraw:
            self.out_win.clear()
        else:
            self.out_win.erase()

        self.out_win.box()
        if title is None:
            self._set_title(self.title)
        else:
            self._set_title(title)            
        
    def debug_clear(self):
        self._clear_window('DEBUG SCREEN')
        self.out_win.refresh()
        
    def debug_write(self, row, col, string):
        self.out_win.addstr(row, col, string)
        self.out_win.refresh()

    def _mark_row(self, row):
        _, cont_sz_x = self.cont_win.getmaxyx()
        self.cont_win.chgat(row, 0, cont_sz_x, curses.A_REVERSE | self._attrib)

    def _draw_vertical_scroll(self):
        pass

    def _center_and_get_start_end(self, row):
        cont_sz_y, _ = self.get_cont_sz()
        start = row - cont_sz_y // 2
        end = start + cont_sz_y
        if start < 0:
            end -= start
            start = 0
        self.first_displayed = start
        return start, end
        
    def _get_start_end(self, row):
        cont_sz_y, _ = self.cont_win.getmaxyx()
        start = self.first_displayed
        end = self.first_displayed + cont_sz_y
        if start <= row < end:
            return start, end
        return self._center_and_get_start_end(row)

    # fixme: simplify this function
    def update_window(self, redraw=False, refresh=False):
        self._clear_window(redraw=redraw)
        for header in self.displayed_headers:
            header_name = self.headers_names[header]
            pos = self.headers_layout[header]['pos']
            self.out_win.addstr(1, pos+1, header_name, curses.A_BOLD)

        sel_row = self.items.get_active_index()
        # cont_sz_y, cont_sz_x = self.get_cont_sz()
        start, end = self._get_start_end(sel_row)
        
        for pos_y, item in enumerate(self.items[start:end], 0):
            for field, layout in self.headers_layout.items():
                pos_x, sz_x = layout['pos'], layout['size']
                string = get_valid_sized_string(item[field], sz_x, True)
                try:  # fixme: handle it better
                    self.cont_win.addstr(pos_y, pos_x, string)
                except Exception:
                    pass

        if sel_row is not None:
            self._mark_row(sel_row - start)
        
        if refresh:
            self.cont_win.refresh()
            self.out_win.refresh()
        else:
            self.cont_win.noutrefresh()
            self.out_win.noutrefresh()

    def next_item(self):
        self.items.select_relative(1)
        self.update_window()

    def prev_item(self):
        self.items.select_relative(-1)
        self.update_window()
